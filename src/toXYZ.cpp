#include "ros/ros.h"
#include "std_msgs/String.h"
#include "sensor_msgs/PointCloud.h"
#include <iostream>
#include <vector>


void generatorCallback(const sensor_msgs::PointCloud msg)
{

  //std::cout << "NODE 0 0 0 0 0 0" << std::endl;

  int numPoints = (int)msg.points.size();

  for (int i = numPoints/2; i < numPoints; i++)
    {
      std::cout << msg.points[i].x << " " <<  msg.points[i].y << " " << msg.points[i].z << std::endl;
    }
  //ROS_INFO("%f", msg.intensity);
  //ROS_INFO("I heard: [%s]", msg->data.c_str());
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "csv_generator");

  ros::NodeHandle n;

  ros::Subscriber sub = n.subscribe("cloud", 1000, generatorCallback);
  ros::spin();

  return 0;
}

