#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <sensor_msgs/PointCloud2.h>
#include <tf/message_filter.h>
#include <message_filters/subscriber.h>
#include <laser_geometry/laser_geometry.h>

#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

class LaserScanToPointCloud2{

public:

	ros::NodeHandle n_,np_;
	laser_geometry::LaserProjection projector_;
	tf::TransformListener listener_;
	message_filters::Subscriber<sensor_msgs::LaserScan> laser_sub_;
	std::string projectToFrame_;
	tf::MessageFilter<sensor_msgs::LaserScan> laser_notifier_;
	ros::Publisher scan_pub_;
	double laserMinRange_, laserMaxRange_;
	double xCropboxMax, xCropboxMin, yCropboxMax, yCropboxMin, zCropboxMax, zCropboxMin;
	bool validCropX, validCropY, validCropZ;

	LaserScanToPointCloud2(ros::NodeHandle n, ros::NodeHandle np, std::string projectToFrame, int bufferFrames) :
		n_(n),
		np_(np),
		laser_sub_(n_, "scan", 100),
		projectToFrame_(projectToFrame),
		laser_notifier_(laser_sub_,listener_, projectToFrame, bufferFrames)
	{

		scan_pub_ = n_.advertise< pcl::PointCloud<pcl::PointXYZ> >("cloud",100);


		double extrapolationLimit;
		if(np_.getParam("extrapolationLimit", extrapolationLimit))
		{
			ROS_INFO_STREAM("setExtrapolationLimit " << extrapolationLimit);
			listener_.setExtrapolationLimit(ros::Duration(extrapolationLimit));
		}


		laser_notifier_.registerCallback(boost::bind(&LaserScanToPointCloud2::scanCallback, this, _1));

		// A transform must exist at the scan timestamp and the tfTolerance after the timestamp in order to qualify for transformation into a point cloud
		double tfTolerance;
		if(np_.getParam("tfTolerance", tfTolerance))
		{
			ROS_INFO_STREAM("setTolerance " << tfTolerance);
			laser_notifier_.setTolerance(ros::Duration(tfTolerance));
		}


		if(!np_.getParam("laser_min_range", laserMinRange_))
		{
			ROS_INFO_STREAM("No modified laser range_min value given. Using original value supplied by driver");
			laserMinRange_  = -1.0;
		}

		if(!np_.getParam("laser_max_range", laserMaxRange_))
		{
			ROS_INFO_STREAM("No modified laser range_max value given. Using original value supplied by driver");
			laserMaxRange_  = -1.0;
		}

		validCropX = false;
		validCropY = false;
		validCropZ = false;
		xCropboxMax = 0.0;
		xCropboxMin = 0.0;
		yCropboxMax = 0.0;
		yCropboxMin = 0.0;
		zCropboxMax = 0.0;
		zCropboxMin = 0.0;

		if(np_.getParam("xCropboxMax", xCropboxMax) && np_.getParam("xCropboxMin", xCropboxMin))
		{
			validCropX = true;
		}
		if(np_.getParam("yCropboxMax", yCropboxMax) && np_.getParam("yCropboxMin", yCropboxMin))
		{
			validCropY = true;
		}
		if(np_.getParam("zCropboxMax", zCropboxMax) && np_.getParam("zCropboxMin", zCropboxMin))
		{
			validCropZ = true;
		}

	}

	void scanCallback (const sensor_msgs::LaserScan::ConstPtr& scan_in)
	{

		sensor_msgs::PointCloud2 cloud;
		sensor_msgs::LaserScan modifiedScan = *scan_in;

		if (laserMinRange_ > 0)
			modifiedScan.range_min = laserMinRange_;

		if (laserMaxRange_ > 0)
			modifiedScan.range_max = laserMaxRange_;


		try
		{
			// range_cutoff: An additional range cutoff which can be applied which is more limiting than max_range in the scan.
			double range_cutoff = -1.0;

//						int mask = 	laser_geometry::channel_option::Intensity |
//								laser_geometry::channel_option::Viewpoint;
//
//			projector_.transformLaserScanToPointCloud(projectToFrame_, modifiedScan, cloud, listener_, range_cutoff, mask );
			projector_.transformLaserScanToPointCloud(projectToFrame_, modifiedScan, cloud, listener_, range_cutoff, laser_geometry::channel_option::None);


			if (validCropX || validCropY || validCropZ)
			{
				pcl::PointCloud<pcl::PointXYZ> pcd, pcd_filtered;
				pcl::fromROSMsg(cloud, pcd);
				cropCloud(pcd, pcd_filtered);
				scan_pub_.publish(pcd_filtered);
			}
			else
				scan_pub_.publish(cloud);

		}
		catch (tf::TransformException& e)
		{
			std::cout << e.what() << std::endl;
			return;
		}

	}

	template <typename PointT> void
	cropCloud(const pcl::PointCloud<PointT> &input_cloud, pcl::PointCloud<PointT> &cloud_filtered)
	{

		cloud_filtered.header = input_cloud.header;
		cloud_filtered.height = 1;


		for (unsigned int i = 0; i < input_cloud.points.size(); i++)
		{
			if (!( (input_cloud.points[i].x < xCropboxMax && input_cloud.points[i].x > xCropboxMin) &&
					(input_cloud.points[i].y < yCropboxMax && input_cloud.points[i].y > yCropboxMin) &&
					(input_cloud.points[i].z < zCropboxMax && input_cloud.points[i].z > zCropboxMin)))
			{
				cloud_filtered.push_back(input_cloud.points[i]);
			}
		}

		cloud_filtered.width = cloud_filtered.points.size();
		//std::cout << "Original size: " << input_cloud.points.size() << " New size: " <<  cloud_filtered.points.size() << std::endl;

		return;
	}

};

int main(int argc, char** argv)
{

	ros::init(argc, argv, "scan_to_cloud_converter_node");
	ros::NodeHandle n;
	ros::NodeHandle np("~");

	std::string projectToFrame;

	if(!np.getParam("project_to_frame", projectToFrame))
	{
		ROS_INFO_STREAM("Missing parameter project_to_frame. Defaulting to /world");
		projectToFrame = "/world";
	}

	int bufferFrames = 100;
	np.getParam("bufferFrames", bufferFrames);

	LaserScanToPointCloud2 lstopc(n, np, projectToFrame, bufferFrames);

	ros::spin();

	return 0;
}
