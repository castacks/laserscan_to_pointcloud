#include "ros/ros.h"
#include "tf/transform_listener.h"
#include "sensor_msgs/PointCloud.h"
#include "tf/message_filter.h"
#include "message_filters/subscriber.h"
#include "laser_geometry/laser_geometry.h"

#define X_MIN -0.5
#define X_MAX 0.5
#define Y_MIN -0.5
#define Y_MAX 0.5
#define Z_MIN -0.5
#define Z_MAX 0.5

class LaserScanToPointCloud{

public:

  ros::NodeHandle n_;
  laser_geometry::LaserProjection projector_;
  tf::TransformListener listener_;
  message_filters::Subscriber<sensor_msgs::LaserScan> laser_sub_;
  tf::MessageFilter<sensor_msgs::LaserScan> laser_notifier_;
  ros::Publisher scan_pub_;

  LaserScanToPointCloud(ros::NodeHandle n) : 
    n_(n),
    laser_sub_(n_, "scan", 10),
    laser_notifier_(laser_sub_,listener_, "local_globescan_world", 10)
  {
    laser_notifier_.registerCallback(boost::bind(&LaserScanToPointCloud::scanCallback, this, _1));
    laser_notifier_.setTolerance(ros::Duration(0.1));
    scan_pub_ = n_.advertise<sensor_msgs::PointCloud>("/my_cloud",1);
  }
  
  void scanCallback (const sensor_msgs::LaserScan::ConstPtr& scan_in)
  {
    sensor_msgs::PointCloud cloud;
    try
      {
        projector_.transformLaserScanToPointCloud("local_globescan_world",*scan_in, cloud,listener_);
      }
    catch (tf::TransformException& e)
      {
        std::cout << e.what();
        return;
      }
    
    // Do something with cloud.

    addColorChannels(cloud);

    //sensor_msgs::PointCloud tunnelCloud = createTunnelVision(cloud);    
    //scan_pub_.publish(tunnelCloud);

    scan_pub_.publish(cloud);
    
  }

  void addColorChannels(sensor_msgs::PointCloud &cloud)  
  {
    int numPoints = (int)cloud.points.size();
    
    sensor_msgs::ChannelFloat32 heightCh;
    heightCh.name = "intensity";

    sensor_msgs::ChannelFloat32 distanceCh;
    distanceCh.name = "intensities";
  

    for (int i = 0; i < numPoints; i++)
      {
	heightCh.values.push_back(cloud.points[i].z);
	distanceCh.values.push_back(sqrt(pow(cloud.points[i].x,2) + pow(cloud.points[i].y,2) + pow(cloud.points[i].z,2)));

	
      }
    //std::cout << "Adding channel\n";
    //cloud.channels.push_back(heightCh);
    cloud.channels.push_back(distanceCh);
  }


  sensor_msgs::PointCloud createTunnelVision(sensor_msgs::PointCloud cloud)
  {
    sensor_msgs::PointCloud tunnelCloud;
    tunnelCloud.header = cloud.header;
    //tunnelCloud.channels[0].string = "index";
    
    int numPoints = (int)cloud.points.size();
    int count = 0;

    for (int i = 0; i < numPoints; i++)
      {
	if (cloud.points[i].x < X_MAX && cloud.points[i].x > X_MIN && 
	    cloud.points[i].y < Y_MAX && cloud.points[i].y > Y_MIN &&
	    cloud.points[i].z < Z_MAX && cloud.points[i].z > Z_MIN)
	  {
	    tunnelCloud.points.push_back(cloud.points[i]);
	    tunnelCloud.channels[0].values.push_back((float)count);
	    count++;
	  }
	
      }
    
    
    //tunnelCloud = cloud;
  return tunnelCloud;
  }
};

int main(int argc, char** argv)
{
  
  ros::init(argc, argv, "my_scan_to_cloud");
  ros::NodeHandle n;
  LaserScanToPointCloud lstopc(n);
  
  ros::spin();
  
  return 0;
}
